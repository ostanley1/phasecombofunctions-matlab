function v = hhbasis(degree, x,y,z, k, mode)
%
% PSBASIS Generates a set of basis functions for a given field of view
%
% BASIS = PSBASIS(DEGREE, PAR, IDX, K)
%
%  DEGREE  -  maximum degree of polynomial basis function
%  SODA    -  field of view orientation and position
%  IDX     -  index of matrix positions to include in basis functions
%  K       -  wavenumber
%

% Copyright 2003-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal 
% in the Software without restriction, includig without limitation the rights 
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
% copies of the Software, and to permit persons to whom the Software is furnished 
% to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included in all 
% copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
% OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
% SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

% Check all the input values
if nargin < 1
   error('HHBASIS requires maximum degree of basis functions.');
elseif nargin < 2
   error('HHBASIS requires acquisition field of view.');
elseif nargin < 3
   % Generate linear voxel index vector to all voxels
   idx = [];
end

besseljs = @(n,z) besselj(n+1/2,z).*sqrt(pi./(2*z));

nidx = length(x);

r = sqrt(x.^2 + y.^2 + z.^2);

phi = atan2(y, x);

% Convert from mm to m
r0 = 1000;

c = z./r;
zidx = isnan(c);
c(zidx) = 0.0;
r = r./r0;

v = ones(nidx, (degree + 1).^2);

r = r .* k;

if strcmp(mode, 'sphere'),
    v(:,1) = ones(size(r));
elseif strcmp(mode, 'solid'),    
    v(:,1) = ones(size(r));
else
    v(:,1) = besseljs(0, r);
end
v(zidx, 1) = 1;

idx = 2;
for n = 1:degree
   if strcmp(mode, 'sphere'),
       radial = ones(size(r));
   elseif strcmp(mode, 'solid'),
       radial = (r./k).^n;
   else
       radial = besseljs(n, r);
   end
   radial(zidx) = 0;
   
   if strcmp(mode, 'radial')
       poly = bsxfun(@times, radial, complex(ones([size(radial, 1) n+1]), zeros([size(radial, 1) n+1])));
   else
       poly = bsxfun(@times, radial, legendre(n, c).');
   end
   
   v(:,idx) = poly(:,1);
   idx = idx + 1;
   for m=1:n,
      if strcmp(mode, 'radial')
          v(:,idx+(0:1)) = bsxfun(@times, ...
                                  poly(:,m+1), ...
                                  complex(ones([size(radial,1), 2]), zeros([size(radial,1), 2])));
      else    
          v(:,idx+(0:1)) = bsxfun(@times, ...
                                  const(n,m) .* poly(:,m+1), ...
                                  [cos(m*phi) sin(m*phi)]);
      end
      idx = idx + 2;
   end
end

% Sort the order to be the same as C++ code
o = cell(size(v,2),1);
for i=0:degree
   o{i+1} = sprintf('A_%d_%d', i, 0);
end
count = degree+2;
for i=1:degree
   for j=i:degree
      o{count} = sprintf('A_%d_%d', j, i);
      o{count+1} = sprintf('A_%d_%d', j, -i);
      count = count + 2;
   end
end
o2 = o;
count = 1;
for i=0:degree
   o{count} = sprintf('A_%d_0', i);
   count = count + 1;
   for j=1:i
      o{count} = sprintf('A_%d_%d', i, j);
      o{count+1} = sprintf('A_%d_%d', i, -j);
      count = count + 2;
   end
end

idx = zeros(numel(o),1);
for i=1:numel(o)
   idx(i) = find(strcmp(o, o2{i}));
end

v = v(:, idx);

return

function v = const(n,m)
v = (-1).^m / sqrt(2) * sqrt( (2*n+1) *factorial(n-m) / factorial(n+m));
