function [names, stats, quality, image, svdimage] = MakefSVDImages(fits, data, mask, slices, Bdata, name, svdimage)
    % Assess quality ratio for each fit in files over a mask
    % Inputs:
    % fits - list of fit structures to generate images for
    % data - data to combine [x,y,z,v,c]
    % mask - mask to assess quality over [x,y,z]
    % slices - array of slices to print
    % Bdata - coords for combined data [x*y*z, 3]
    % name - output base name for pictures
    % svdimage - svd combined image [x*y*z,1]
    % Outputs:
    % names - output files prefixes (1 per input fit)
    % stats - statistics
    % qualtiy - quality ratio image [x,y,z]
    % image - combined image [x,y,z]
    % svdimage - image used for svd comparison [x,y,z]
    
    

    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.
    
    [x,y,z,v,c]=size(data);
    % create target set from svd
    
    if nargin < 7
        data = reshape(double(data), [], v, c);
        sens = complex(zeros(x*y*z,c), zeros(x*y*z,c));
        parfor i=1:size(data,1),
            [U,S,V]=svd(squeeze(data(i,2:end,:)));
            sens(i,:) = sqrt(S(1,1)).*V(:,1).*U(1,1)./abs(U(1,1));
        end
        svdimage = sum(squeeze(data(:,1,:)).*sens./abs(sens),2);
        data = reshape(data(:,1,:), [], c);
    else
        data = reshape(data(:,:,:,1,:), [], c);
    end
    
    
    quality_array = zeros(size(fits,1),5);
    
    names = [];
  
    for f=1:size(fits,2)

        basisfit = hhbasis(fits(f).order, Bdata(:,1), Bdata(:,2), Bdata(:,3), 1, 'solid');

        shimhull = convhulln(double(fits(f).coords(fits(f).mask>fits(f).fit_mask,:)));
        imhull = inhull(Bdata, fits(f).coords(fits(f).mask>fits(f).fit_mask,:), shimhull);
        [~, idx] = bwdist(reshape(imhull,size(mask,1), size(mask,2), size(mask,3)));
        
        Mcalc=basisfit*squeeze(fits(f).coil_weights);
        Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
        image = sum(squeeze(data).*Mcalc./abs(Mcalc),2);
        quality = abs(image)./abs(svdimage);

        image = reshape(image,size(mask));
        quality = reshape(quality,size(mask));
        namebase = strcat(name,'order', int2str(fits(f).order), 'fitthr', int2str(fits(f).fit_mask),...
            'minimaxthr', int2str(fits(f).minimax_mask))
        figure('Position', [10 10 900 600])
        figmri(image(:,:,slices).*mask(:,:,slices), [], 'gray')
        colorbar('off')
        set(gca, 'XColor', 'none', 'YColor', 'none')
        print(gcf, strcat(namebase, 'mag.png'), '-dpng', '-r450')
        
        figure('Position', [10 10 900 600])

        figmri(angle(image(:,:,slices)), [], 'gray')
        colorbar('off')
        set(gca, 'XColor', 'none', 'YColor', 'none')
        print(gcf, strcat(namebase, 'phase.png'), '-dpng', '-r450')
        
        phaseuw = unwrap3dA(single(angle(image)), mask>0);
        figure('Position', [10 10 900 600])
        figmri(phaseuw(:,:,slices).*mask(:,:,slices), [-10, 10], 'gray')
        colorbar('off')
        set(gca, 'XColor', 'none', 'YColor', 'none')
        print(gcf, strcat(namebase, 'phaseuw.png'), '-dpng', '-r450')
        
        figure('Position', [10 10 900 600])
        figmri(quality(:,:,slices).*mask(:,:,slices), [0,1])
        colorbar('off')
        set(gca, 'XColor', 'none', 'YColor', 'none')
        print(gcf, strcat(namebase, 'quality.png'), '-dpng', '-r450')
        
        names = [names, namebase];
        quality = abs(image)./abs(reshape(svdimage, size(image)));
        
        magnitude = abs(svdimage);
        cleaned_mask = (mask(:)>0).*(magnitude(:)>(0.03*median(magnitude(:))));
        stats = [mean(quality(cleaned_mask(:)>0)) std(quality(cleaned_mask(:)>0))];
        stats = [stats stats(2)/stats(1)*100]
    end
    
end

