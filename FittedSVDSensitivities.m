function fitresult = FittedSVDSensitivities( data, coords, input_params, name, varargin ) 
    %FittedSVDSensitivities find fitted svd sensitivities
    %   data [x,y,z,v,c]
    %   coords [x*y*z, 3]
    %   input_params [order, fit_mask, minimaxmask]
    %   name of the data to include
    %   minimax weights
    
    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.
    
    input_params
    fitresult.order = input_params(1);
    fitresult.fit_mask = input_params(2);
    fitresult.minimax_mask = input_params(3);
    fitresult.coords = coords;
    fitresult.name = name;
    fitresult.minimax_weights = input_params(3);
    [x,y,z,v,c]=size(data);

    %perform SVD to get rel sens
    data = reshape(double(data), [], v, c);
    sens = complex(zeros(x*y*z, c), zeros(x*y*z, c));
    sdiag = zeros(x*y*z,min(c,v));
    parfor i=1:size(data,1),
        [U,S,V]=svd(squeeze(data(i,:,:)),'econ');
        sens(i,:) = sqrt(S(1,1)).*V(:,1).*U(1,1)./abs(U(1,1));
        sdiag(i,:)=diag(S);
    end
    fitresult.mask = sdiag(:,1)./sdiag(:,2);
    fitresult.minimax_mask
    
    %perform minimax
    if length(varargin)==0
        options = optimoptions('fminimax', 'MaxFunEvals', 1000*32,...
            'MaxIter', 4000, 'TolCon', 1e-6,...
            'RelLineSrchBnd', 0.1, 'UseParallel', true,'Display', 'iter') 
        mask = sdiag(:,1)./sdiag(:,2)>fitresult.minimax_mask;
        if sum(mask) == 0
            fitresult.error = 'Minimax mask too tight'
            return
        end
        relB1 = bsxfun(@rdivide, sens(mask>0,:), sqrt(sdiag(mask>0,1)));
        [~,~,V]=svd(relB1, 'econ');

        f = @(x) -abs(relB1*complex(x(1:ceil(length(x)/2)),...
                                    x(ceil(length(x)/2)+1:end)));
        x0 = [real(V(:,1)); imag(V(:,1))];
        [result,~,minval,exitflag] = fminimax(f,x0, [],[],[],[],[],[],@minimaxconst,options);
        minimaxweights = complex(result(1:ceil(length(result)/2)),...
                                result(ceil(length(result)/2)+1:end));
    else
        minimaxweights = varargin{1};
    end
    fitresult.minimax_weights = minimaxweights;
    %perform iterative fitting
    mask = sdiag(:,1)./sdiag(:,2)>fitresult.fit_mask;    
    if sum(mask) == 0
        fitresult.error = 'fit mask too tight'
        return
    end
    gs = sum(bsxfun(@times, sens(mask,:), minimaxweights.'), 2);
    wsens = bsxfun(@times, sens(mask,:), conj(gs));
    [fitresult.coil_weights, ~, basisfit, ~, t ] = runiterativefit( fitresult.order, [], wsens, sqrt(sdiag(:,1)).*mask, coords);
    
    %create complex hull and apply fit
    shimhull = convhulln(double(coords(mask,:)));
    imhull = inhull(coords, coords(mask,:), shimhull);
    [D, idx] = bwdist(reshape(imhull,x,y,z));
   
    Mcalc=basisfit*fitresult.coil_weights;
    Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
    image = sum(squeeze(data(:,1,:)).*Mcalc./abs(Mcalc),2);
    svdimage = sum(squeeze(data(:,1,:)).*sens./abs(sens),2);
    quality = abs(image)./abs(svdimage);
    
    fitresult.shimquality = mean(quality(mask>0));
end

