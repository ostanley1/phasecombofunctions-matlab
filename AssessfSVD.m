function [quality_array] = AssessfSVD(fits, data, mask, Bdata, svdimage, plotlist)
    % Assess quality ratio for each fit in files over a mask
    % fits - list of fit structures to assess
    % data - data to combine [x,y,z,v,c]
    % mask - mask to assess quality over [x,y,z]
    % Bdata - coords for combined data [x*y*z, 3]
    % svdimage - input svd combined image [x*y*z,1]
    % plotlist - list of orders to plot [o, fit_mask, minimax_mask]
    
    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.

    [x,y,z,v,c]=size(data);
    
    if (nargin < 5) || (length(svdimage)==0)
        data = reshape(double(data), [], v, c);
        sens = complex(zeros(x*y*z,c),zeros(x*y*z,c));
        parfor i=1:size(data,1),
            [U,S,V]=svd(squeeze(data(i,2:end,:)));
            sens(i,:) = sqrt(S(1,1)).*V(:,1).*U(1,1)./abs(U(1,1));
        end
        svdimage = sum(squeeze(data(:,1,:)).*sens./abs(sens),2);
        data = reshape(data(:,1,:), [], c);
    else
        data = reshape(data(:,:,:,1,:), [], c);
    end
    
    quality_array = zeros(size(fits,1),5);
    name = fits(1).name;
    
    for f=1:size(fits,2)
        f
        if isfield(fits(f),'error')
            quality_array(f,:)=[fits(f).order, fits(f).minimax_mask, fits(f).fit_mask, -1, -1]
            continue
        end
        assert(strcmp(fits(f).name,name));
       
        basisfit = hhbasis(fits(f).order, Bdata(:,1), Bdata(:,2), Bdata(:,3), 1, 'solid');

        shimhull = convhulln(double(fits(f).coords(fits(f).mask>fits(f).fit_mask,:)));
        imhull = inhull(Bdata, fits(f).coords(fits(f).mask>fits(f).fit_mask,:), shimhull);
        [~, idx] = bwdist(reshape(imhull,size(mask,1), size(mask,2), size(mask,3)));
        
        Mcalc=basisfit*squeeze(fits(f).coil_weights);
        Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
        image = sum(data.*Mcalc./abs(Mcalc),2);
        quality = abs(image)./abs(svdimage);
                
        if nargin > 5
            if ismember([fits(f).order,fits(f).fit_mask, fits(f).minimax_mask], plotlist, 'row')
                figure
                subplot(121)
                figmri(reshape(image,size(mask)))
                title([fits(f).order,fits(f).fit_mask, fits(f).minimax_mask]')
                subplot(122)
                figmri(reshape(quality,size(mask)), [0 1])

                quality = reshape(quality,size(mask));
                image = reshape(image,size(mask));
                figure
                figmri(image)
                print(strcat(fits(f).name,'magnitude.png'), '-dpng')
                figure
                figmri(angle(image))
                print(strcat(fits(f).name,'phase.png'), '-dpng')
                figure
                figmri(quality, [0 1])
                print(strcat(fits(f).name,'quality.png'), '-dpng')
            end
        end
        magnitude = abs(svdimage);
        cleaned_mask = (mask(:)>0).*(magnitude(:)>(0.03*median(magnitude(:))));
        quality_array(f,:)=[fits(f).order, fits(f).minimax_mask, fits(f).fit_mask, mean(quality(cleaned_mask(:)>0)), std(quality(cleaned_mask(:)>0))];
        quality_array(f,:)
    end
end

