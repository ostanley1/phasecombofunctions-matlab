function [coords,R] = calcpositions_ismrmrd( hd )
    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.

    % based on nipy.org/nibabel/dicom/dicom_orientation.html
    % calculate offset free affine and coords then apply offset and find
    % real affine
    voxel_size = hd.field_of_view(:,1)./double(hd.matrix_size(:,1));
    % the slice_dir and voxel size do not represent the z affine due
    % to interslice spacing. To work around this the zdir is calculated
    % from the positions of the slices. This will fail if slice order is
    % descending
    slice_positions = unique(hd.position','rows')';
    zdir = (slice_positions(:,2)-slice_positions(:,1));
    R = [hd.read_dir(:,1)*voxel_size(1), hd.phase_dir(:,2)*voxel_size(2), zdir, slice_positions(:,1)];
    R(4,:)=[0 0 0 1];
    
    % need to adjust offsets for x and y to side of slice, z is generated
    % from bottom of slice and so just needs any adjustments from the x and
    % y translation
    adj_offset = R*[-double(hd.matrix_size(1:2,1))/2; 0; 1];
    R(1:3,4)=adj_offset(1:3,1);
    
    [X,Y,Z] = meshgrid((1:hd.matrix_size(2,1)),...
        (1:hd.matrix_size(1,1)),...
        (1:length(slice_positions)));
    % move data to center of voxel in x and y and bottom of slice in z, dcm
    % standard
    coords = [reshape(double(X),1,[])-0.5; reshape(double(Y),1,[])-0.5; reshape(double(Z),1,[])-1; ones([1,numel(X)])];
    coords = R*coords ; 
    
    coords = coords(1:3,:)';
end
