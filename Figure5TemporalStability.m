% Generates all figures for Figure 6. This data is loaded in line by line
% but this script is still memory intensive, use large compute cluster.
% This script needs to berun twice for full affect, the first time
% calculates the sensitivities with voxelwise SVD and all combos except the
% fitted SVD method with a matched image set. The second time it should run
% all the combinations including the matched image fitted SVD method using
% the sensitivities from the previous voxelwise svd
% 
% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% load in shim data
path(path, ['./BCC_ESPIRiT_Coil_Combine_Toolbox/bart-0.2.06/'])
path(path, ['./BCC_ESPIRiT_Coil_Combine_Toolbox/bart-0.2.06/matlab/'])
path(path, ['./BCC_ESPIRiT_Coil_Combine_Toolbox/'])

base_dir = '../Dataset2OccipitalCoil/'
%%
filelist = {strcat(base_dir,'RelB1Data_opcoil.ismrmrd');...
    strcat(base_dir,'AbsB1Data_opcoil.ismrmrd')}

[shimdata, shimhdr, Bshim, Rshim] = loaddata_ismrmrd(filelist,'/data/images/data','/data/images/header');

%% Call FittedSVDSensitivities 
o = 6
fit_mask = 20 
minimax_mask = 20 

fitresults = FittedSVDSensitivities(shimdata, Bshim, [o, fit_mask, minimax_mask], 'Figure4OPCoil')
        
%% Load in test data line by line to save memory
%load in first volume for reference
filelist = strcat(base_dir, 'TargetEPIData.ismrmrd');

[firstvol, hd, fullbasis, ~] = loaddata_ismrmrd(filelist,'/images/complex/data','/images/complex/header', 1,3);
[x,y,z,v,c]=size(firstvol)
v=240;
%%
% Calculate fSVD solution so it can be applied line by line
basisfit = hhbasis(fitresults.order, fullbasis(:,1), fullbasis(:,2), fullbasis(:,3), 1, 'solid');

shimhull = convhulln(double(fitresults.coords(fitresults.mask>fitresults.fit_mask,:)));
imhull = inhull(fullbasis, fitresults.coords(fitresults.mask>fitresults.fit_mask,:), shimhull);
[~, idx] = bwdist(reshape(imhull,x,y,z));

Mcalc=basisfit*squeeze(fitresults.coil_weights);
Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
Mcalc_full = Mcalc;
Mcalc = reshape(permute(reshape(Mcalc, x,y,z,c),[1,3,2,4]), [], y, c);

%% Calculate virtual coil from first volume for later application
[~, ~, virtcoil] = VRCCombination(squeeze(firstvol));

phasediff = reshape(bsxfun(@times, conj(virtcoil), squeeze(firstvol)),[x,y,z,c]);

filtreal = zeros(size(phasediff));
filtimag = zeros(size(phasediff));
for k=1:size(phasediff,4),
        filtreal(:,:,:,k) = imgaussfilt3(real(phasediff(:,:,:,k)),10);
        filtimag(:,:,:,k) = imgaussfilt3(imag(phasediff(:,:,:,k)),10);
end
sens_vrc = reshape(permute(complex(filtreal, filtimag), [1,3,2,4]), [x*z,y,c]);

       
%% BCC Combination for later application
img = reshape(firstvol, 104,104,54,32);

N = size(img(:,:,:,1));
block_size = [1,1,N(3)];
rot_disp = [90,90,-90];
[ img_svd, Cmp_Blox ] = bcc_extractBlock( img, block_size );

% align phase in readout

[ img_align, Cmp_Align ] = bcc_alignRO( img, Cmp_Blox, block_size );

imagesc3d2(angle(img_align), N/2, 12, rot_disp, [-pi,pi])

% align phase in phase encode

[ img_align2, Cmp_Align2  ] = bcc_alignPE( img, Cmp_Align, block_size );

imagesc3d2(angle(img_align2), N/2, 22, rot_disp, [-pi,pi])

% align phase in partition encode

[ img_align3, Cmp_Align3 ] = bcc_alignPAR( img, Cmp_Align2, block_size );

imagesc3d2(angle(img_align3), N/2, 32, rot_disp, [-pi,pi])

% ESPIRiT requires BART -> install version 0.2.06 and add to path

cd BCC_ESPIRiT_Coil_Combine_Toolbox/bart-0.2.06/
bart_path = [pwd];
%     system('make')
cd ../..

setenv('TOOLBOX_PATH', bart_path)
addpath(strcat(getenv('TOOLBOX_PATH'), '/matlab'));
setenv('PATH', strcat(getenv('TOOLBOX_PATH'), ':', getenv('PATH')));
setenv('LD_LIBRARY_PATH', '');


% ESPIRiT with virtual body coil as phase reference

num_acs = 16;       % size of calibration k-space fopr ESPIRiT
c = 0.2             % threshold for sensitivity mask    


% tic
    % concatenate virtual body coil as channel 1 to serve as phase
    % reference, but weight its magnitude by 1e-6 to preserve parallel imaging
    writecfl('img', single( cat(4, 1e-6 * img_align3, img) ))              

    system('fft 7 img kspace')

    system(['ecalib -c ', num2str(c), ' -r ', num2str(num_acs), ' kspace calib'])

    system('slice 4 0 calib senstemp')
% toc


% clean up space:
system('rm calib.hdr')
system('rm calib.cfl')

system('rm kspace.hdr')
system('rm kspace.cfl')

system('rm img.hdr')
system('rm img.cfl')

c=32;
sens_bcc = single(readcfl('senstemp'));

% grab only the head array sensitivities
sens_bcc = sens_bcc(:,:,:,2:end);      
sens_bcc = reshape(permute(reshape(sens_bcc, x,y,z,c),[1,3,2,4]), [], y, c);

%% load in full svd from visual coil and calculate fitted SVD method
if isfile(strcat(base_dir, 'TemporalNoise.mat'))
    load(strcat(base_dir, 'TemporalNoise.mat'), 'sdiag', 'sens')
    sdiag=reshape(permute(reshape(sdiag, x,z,y,2), [1,3,2,4]),x*y*z,2);
    sens=reshape(permute(reshape(sens, x,z,y,c), [1,3,2,4]),x*y*z,c);

    fitresult.order = 6;
    fitresult.fit_mask = 20;
    fitresult.minimax_mask = 20;
    fitresult.coords = fullbasis;
    fitresult.name = 'Dataset2FullImageFit';
    fitresult.mask = sdiag(:,1)./sdiag(:,2);

    options = optimoptions('fminimax', 'MaxFunEvals', 1000*32,...
                'MaxIter', 4000, 'TolCon', 1e-6,...
                'RelLineSrchBnd', 0.1, 'UseParallel', true,'Display', 'iter') 
    mask = sdiag(:,1)./sdiag(:,2)>fitresult.minimax_mask;
    if sum(mask) == 0
        fitresult.error = 'Minimax mask too tight'
        return
    end
    relB1 = bsxfun(@rdivide, sens(mask>0,:), sqrt(sdiag(mask>0,1)));
    [~,~,V]=svd(relB1, 'econ');

    f = @(x) -abs(relB1*complex(x(1:ceil(length(x)/2)),...
                                x(ceil(length(x)/2)+1:end)));
    x0 = [real(V(:,1)); imag(V(:,1))];
    [result,~,minval,exitflag] = fminimax(f,x0, [],[],[],[],[],[],@minimaxconst,options);
    minimaxweights = complex(result(1:ceil(length(result)/2)),...
                            result(ceil(length(result)/2)+1:end));
    fitresult.minimax_weights = minimaxweights;
    %perform iterative fitting
    mask = sdiag(:,1)./sdiag(:,2)>fitresult.fit_mask;    
    if sum(mask) == 0
        fitresult.error = 'fit mask too tight'
        return
    end
    gs = sum(bsxfun(@times, sens(mask,:), minimaxweights.'), 2);
    wsens = bsxfun(@times, sens(mask,:), conj(gs));
    [fitresult.coil_weights, ~, basisfit, ~, t ] = runiterativefit( fitresult.order, [], wsens, sqrt(sdiag(:,1)).*mask, fitresult.coords);

    % create complex hull and apply fit
    shimhull = convhulln(double(fitresult.coords(mask,:)));
    imhull = inhull(fitresult.coords, fitresult.coords(mask,:), shimhull);
    [D, idx] = bwdist(reshape(imhull,x,y,z));

    Mcalcfull=basisfit*fitresult.coil_weights;
    Mcalcfull(imhull~=1,:)=Mcalcfull(idx(imhull~=1),:);
    Mcalcfull = reshape(permute(reshape(Mcalcfull, x,y,z,c),[1,3,2,4]), [], y, c);
end
%% generate initial arrays
sens = complex(zeros(x*z,y,c), zeros(x*z,y,c));
sdiag = complex(zeros(x*z,y,2), zeros(x*z,y,2));
fSVD_ts = complex(zeros(x*z,y,v), zeros(x*z,y,v));
fullfSVD_ts = complex(zeros(x*z,y,v), zeros(x*z,y,v));
vSVD_ts = complex(zeros(x*z,y,v), zeros(x*z,y,v));
complexsum_ts = complex(zeros(x*z,y,v), zeros(x*z,y,v));
VRC_ts = complex(zeros(x*z,y,v), zeros(x*z,y,v));
BCC_ts = complex(zeros(x*z,y,v), zeros(x*z,y,v));
n = 4;

% for each chunk:
% load it in
% get relevant svd sens
% combine vsvd solution
% combine fsvd solution
for l=1:104/n,
    a=tic;
    l
    [imdata, imhdr, Bim, Rim] = loaddata_ismrmrd(filelist,'/images/complex/data','/images/complex/header', v, 3, (l-1)*n+1, n);
    
    for line=1:n
        imdata_chunk=reshape(imdata(:, line, :,:,:),[],v,c);
        for i=1:size(imdata_chunk,1)
            [U,S,V]=svd(squeeze(imdata_chunk(i,:,:)));
            sens(i,(l-1)*n+line,:) = V(:,1).*U(1,1)./abs(U(1,1));
            sd = diag(S);
            sdiag(i,(l-1)*n+line,:) = sd(1:2);
        end
    end
    
    imdata = reshape(permute(imdata,[1,3,2,5,4]), x*z, n, c, v);
    range=(l-1)*n+1:l*n;
    vSVD_ts(:,range,:) = squeeze(sum(bsxfun(@times,imdata, sens(:,range,:)./abs(sens(:,range,:))),3));
    fSVD_ts(:,range,:) = squeeze(sum(bsxfun(@times,imdata, Mcalc(:,range,:)./abs(Mcalc(:,range,:))),3));
    if isfile(strcat(base_dir, 'TemporalNoise.mat'))
        fullfSVD_ts(:,range,:) = squeeze(sum(bsxfun(@times,imdata, Mcalcfull(:,range,:)./abs(Mcalcfull(:,range,:))),3));    
    end
    BCC_ts(:,range,:) = squeeze(sum(bsxfun(@times,imdata, conj(sens_bcc(:,range,:))./abs(sens_bcc(:,range,:))),3));
    VRC_ts(:,range,:) = squeeze(sum(bsxfun(@times,imdata, conj(sens_vrc(:,range,:))./abs(sens_vrc(:,range,:))),3));
    complexsum_ts(:,range,:) = squeeze(sum(imdata,3));
   
    
    if mod(l, 10)==0
        save(strcat(base_dir, 'TemporalNoise.mat'), 'sens','sdiag','vSVD_ts','fSVD_ts',...
            'complexsum_ts', 'VRC_ts', 'BCC_ts','fullfSVD_ts', '-v7.3')
    end
    toc(a)
end

%% Save data
save(strcat(base_dir, 'TemporalNoise.mat'), 'sens', 'sdiag', 'vSVD_ts','fSVD_ts',...
            'complexsum_ts', 'VRC_ts', 'BCC_ts','fullfSVD_ts', '-v7.3')

%% Load in and confirm mask orientation
base_dir = '../Dataset2OccipitalCoil/'
mask= niftiread(strcat(base_dir, 'OPCoilDataBrainMask.nii.gz'));
mask(:,:,1:10)=0;
mask(:,:,51:end)=0;
mask=double(rot90(mask,-1));
 
% Plot figures
load(strcat(base_dir, 'TemporalNoise.mat'))
%%
[x,y,z]=size(mask)
v=240
if length(size(vSVD_ts))<4
    vSVD_ts = permute(reshape(vSVD_ts, x,z,y,v), [1,3,2,4]);
end
[name, stats]= MakeTemporalImages(permute(reshape(fSVD_ts, x,z,y,v), [1,3,2,4]), mask, 35:3:45, 'Figures/Figure6TstdfSVD.png', vSVD_ts)
[name, stats]= MakeTemporalImages(vSVD_ts, mask, 35:3:45, 'Figures/Figure6TstdvSVD.png', vSVD_ts);
[name, stats, cs_proc]= MakeTemporalImages(permute(reshape(complexsum_ts, x,z,y,v), [1,3,2,4]), mask, 35:3:45, 'Figures/Figure6Tstdcs.png', vSVD_ts)
[name, stats]= MakeTemporalImages(permute(reshape(VRC_ts, x,z,y,v), [1,3,2,4]), mask, 35:3:45, 'Figures/Figure6TstdVRC.png', vSVD_ts)

BCC_ts(isnan(BCC_ts))=-100000;
[name, stats]= MakeTemporalImages(permute(reshape(BCC_ts, x,z,y,v), [1,3,2,4]), mask, 35:3:45, 'Figures/Figure6TstdBCC.png', vSVD_ts);
[name, stats]= MakeTemporalImages(permute(reshape(fullfSVD_ts, x,z,y,v), [1,3,2,4]), mask, 35:3:45, 'Figures/Figure6TstdfullfSVD.png', vSVD_ts)
