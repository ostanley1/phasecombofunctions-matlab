function [combdata_vrc, refind, virtcoil] = VRCCombination(data)
%VRCCOMBINATION Performs VRC combination on input data in form x,y,z,c
%   outputs combined data, ref voxel, virtual coil

% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.


%find the index of the voxel with the maximum shared signal
minimg = min(abs(squeeze(data)), [], 4);
voxmax = max(minimg(:));
refvox=find(minimg==voxmax)
[xref,yref,zref]=ind2sub(size(minimg), refvox)
refind = [xref,yref,zref];

%create reference coil and subtract from data
refphase = data(xref,yref,zref,:)./abs(data(xref,yref,zref,:));
virtcoil = sum(bsxfun(@times,conj(refphase),data),4)./sum(abs(data),4);
phasediff = bsxfun(@times, conj(virtcoil), data);

% smooth phase differences
filtreal = zeros(size(phasediff));
filtimag = zeros(size(phasediff));
for k=1:size(data,4),
        filtreal(:,:,:,k) = imgaussfilt3(real(phasediff(:,:,:,k)),10);
        filtimag(:,:,:,k) = imgaussfilt3(imag(phasediff(:,:,:,k)),10);
end
filtphasediff = complex(filtreal, filtimag);

% combine data
combdata_vrc = data.*conj(filtphasediff)./abs(filtphasediff);

end

