function h = figmri(map,clim,cmap,showbar,horizon,ticks,orient)

% FIGMRI displays a color MRI image and returns the handle of the figure
% [handle] = figmri(image, clim, cmap, showbar, horizon, ticks, orient)

% Copyright 2003-2021 L. Martyn Klassen
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal 
% in the Software without restriction, includig without limitation the rights 
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
% copies of the Software, and to permit persons to whom the Software is furnished 
% to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included in all 
% copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
% OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
% SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

if nargin < 7
   orient = 'e';
	if nargin < 6
      ticks = 1;
		if nargin < 5
         horizon = 0;
         if nargin < 4
            showbar = 1;
            if nargin < 3;
               cmap = colormap;
               if nargin < 2
                  clim = [];
               end
            end
         end
		end
	end
end

% Convert complex values to magnitude
if ~isreal(map)
   map = abs(map);
end

% Squeeze out any blank dimensions
map = squeeze(map);
ndim = ndims(map);
if ndim > 3
   map = permute(map, [1 3 2 4:ndim]);
   siz = size(map);
   map = reshape(map, [prod(siz(1:2)) prod(siz(3:ndim))]);
elseif ndim == 3
   siz = size(map);
   switch lower(orient(1))
      case 'h'
         n = 1;
      case 'v'
         n = siz(3);
      otherwise
         for n = floor(sqrt(siz(3))):-1:1 %#ok<PFUIX,FORFLG>
            if rem(siz(3),n) == 0 %#ok<PFBNS>
               break;
            end
         end
   end      
   map = reshape(map, [siz(1:2) n siz(3)/n]);
   map = permute(map, [1 3 2 4]);
   siz = size(map);
   siz(end+1:4) = 1;
   map = reshape(map, [prod(siz(1:2)) prod(siz(3:4))]);
else
   map = map(:,:);
end

colormap(cmap);

if isempty(clim)
   h = imagesc(map);
else
   h = imagesc(map, clim);
end
axis image;
if showbar
   if horizon
      colorbar('horiz');
   else
      colorbar;
   end
end
set(gca, 'FontSize', 18)
if ~ticks
   set(gca, 'XTickLabel', [], 'YTickLabel',[], 'XTick', [], 'YTick', []);
   
end

if nargout < 1
   clear h;
end
