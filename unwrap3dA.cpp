#include "matrix.h"
#include "unwrap3D.c"
#include "mex.h"


// Copyright 2003-2021 L. Martyn Klassen
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal 
// in the Software without restriction, includig without limitation the rights 
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
// copies of the Software, and to permit persons to whom the Software is furnished 
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all 
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   //the main function of the unwrapper
	float *WrappedVolume, *UnwrappedVolume;
	unsigned char *input_mask, *extended_mask;
   int No_of_edges=0;

   if (nlhs != 1)
      mexErrMsgIdAndTxt("CFMM:PhaseUnwrap:nlhs", "One output required.");
   if (nrhs != 2)
      mexErrMsgIdAndTxt("CFMM:PhaseUnwrap:nrhs", "Two inputs required.");
   
   if (!mxIsSingle(prhs[0]) || mxIsComplex(prhs[0]))
      mexErrMsgIdAndTxt("CFMM:PhaseUnwrap:notSingle", "Phase must be 3D floating point.");
   
   if (!mxIsLogical(prhs[1]))
      mexErrMsgIdAndTxt("CFMM:PhaseUnwrap:notLogical", "Mask must be logical.");
   
   const mwSize *dims = mxGetDimensions(prhs[0]);
   const mwSize ndim = mxGetNumberOfDimensions(prhs[0]);
   
   if (ndim != 3) 
      mexErrMsgIdAndTxt("CFMM:PhaseUnwrap:dimension", "Phase must be 3D.");

   if (mxGetNumberOfDimensions(prhs[1]) != ndim)
      mexErrMsgIdAndTxt("CFMM:PhaseUnwrap:dimensionMismatch", "Size of inputs are different.");
   else {
      const mwSize *dims2 = mxGetDimensions(prhs[1]);
      for (int i=0; i<ndim; i++) {
         if (dims[i] != dims2[i]) {
            mexErrMsgIdAndTxt("CFMM:PhaseUnwrap:dimensionMismatch", "Size of inputs are different.");
         }
      }
   }

   input_mask = (unsigned char *) mxGetPr(prhs[1]);
   WrappedVolume = (float *) mxGetPr(prhs[0]);
   
   plhs[0] = mxCreateNumericArray(ndim, dims, mxSINGLE_CLASS, mxREAL);
   UnwrappedVolume = (float *) mxGetPr(plhs[0]);
   
	int volume_width = dims[0];
	int volume_height = dims[1];
	int volume_depth = dims[2];
	int volume_size = volume_height * volume_width * volume_depth;
	int No_of_Edges_initially = 3 * volume_width * volume_height * volume_depth;

	extended_mask = (unsigned char *) mxCalloc(volume_size, sizeof(unsigned char));
	
	VOXELM *voxel = (VOXELM *) mxCalloc(volume_size, sizeof(VOXELM));
	EDGE *edge = (EDGE *) mxCalloc(No_of_Edges_initially, sizeof(EDGE));;

   //mexPrintf("DEBUG %d\n",__LINE__);
	extend_mask(input_mask, extended_mask, volume_width, volume_height, volume_depth);
	
   //mexPrintf("DEBUG %d\n",__LINE__);
	initialiseVOXELs(WrappedVolume, input_mask, extended_mask, voxel, volume_width, volume_height, volume_depth);

   //mexPrintf("DEBUG %d\n",__LINE__);
	calculate_reliability(WrappedVolume, voxel, volume_width, volume_height, volume_depth);

   //mexPrintf("DEBUG %d\n",__LINE__);
	horizentalEDGEs(voxel, edge, &No_of_edges, volume_width, volume_height, volume_depth);

   //mexPrintf("DEBUG %d\n",__LINE__);
	verticalEDGEs(voxel, edge, &No_of_edges, volume_width, volume_height, volume_depth);

   //mexPrintf("DEBUG %d\n",__LINE__);
	normalEDGEs(voxel, edge, &No_of_edges, volume_width, volume_height, volume_depth);

	//sort the EDGEs depending on their reiability. The VOXELs with higher relibility (small value) first
	//run only one of the two functions (quick_sort() or quicker_sort() )
	//if your code stuck because of the quicker_sort() function, then use the quick_sort() function
   //mexPrintf("DEBUG %d\n",__LINE__);
	quick_sort(edge, No_of_edges);
	//quicker_sort(edge, edge + No_of_edges - 1);

	//gather VOXELs into groups
   //mexPrintf("DEBUG %d\n",__LINE__);
	gatherVOXELs(edge, No_of_edges);

   //mexPrintf("DEBUG %d\n",__LINE__);
	unwrapVolume(voxel, volume_width, volume_height, volume_depth);

   //mexPrintf("DEBUG %d\n",__LINE__);
	maskVolume(voxel, input_mask, volume_width, volume_height, volume_depth);

	//copy the volume from VOXELM structure to the unwrapped phase array passed to this function
   //mexPrintf("DEBUG %d\n",__LINE__);
	returnVolume(voxel, UnwrappedVolume, volume_width, volume_height, volume_depth);

   //mexPrintf("DEBUG %d\n",__LINE__);
	mxFree(edge);
	mxFree(voxel);
	mxFree(extended_mask);
}
