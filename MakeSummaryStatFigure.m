function name = MakeSummaryStatFigure(summary_im, linemask, xvalues, yvalues, cbarticks,...
                                      panellabel, xname, yname, cbarlabel, name)
                                  
    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.                            
                                  
    figure('Position', [10 10 900 600])
    imshow(summary_im, [min(cbarticks), max(cbarticks)], 'Colormap', jet(256),...
        'InitialMagnification','fit', ...
        'XData', 1:length(xvalues{1}), 'YData', 1:length(yvalues{1}))
    axis on
    if length(linemask)~=0
        [B,L] = bwboundaries(linemask, 4, 'noholes');
        hold on
        for k = 1:length(B)
           boundary = B{k};
           plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
        end
    end
    set(gca, 'FontSize', 18)
    set(gca, 'TickLength', [0 0])
    xticks('manual')
    xticklabels(xvalues)
    yticks('manual')
    yticklabels(yvalues)

    ylabel(yname, 'FontSize', 24)
    xlabel(xname, 'FontSize', 24)
    annotation('textbox', [0.1, 0.95, 0, 0], 'string', panellabel, 'FontSize', 24)

    hcb = colorbar('FontSize', 24, 'TickLength', 0, 'Ticks', [cbarticks])
    set(get(hcb,'YLabel'),'String', cbarlabel, 'FontSize', 24)
    saveas(gcf, name)
end

