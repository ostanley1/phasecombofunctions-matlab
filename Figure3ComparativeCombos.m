% Runs and times all comparitive combos for Figure 3. To run all timing set 
% timing_mode to 1.
% 
% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.


timing_mode = 0 %change if you wish to time all combos
for i=1:5    
    % Load in target images and calulate SVD derived solution for
    % denominator

    base_dir = '../Dataset1ComparativeCombos/'

    compare_string = strcat(base_dir, 'TargetGREData%i_compare.ismrmrd');
    for i=1:5
        compare{i,1}=sprintf(compare_string, i);
    end

    [imdata_comp, imhdr, Bim, Rim] = loaddata_ismrmrd(compare,'/images/complex/data','/images/complex/header');
    imdata = imdata_comp(:,:,:,1,:);
    [x,y,z,v,c]=size(imdata);


    mask = rot90(niftiread(strcat(base_dir, 'ComparativeComboBrainMask.nii.gz')),-1);

    % SVD voxelwise
    imdata_comp = reshape(double(imdata_comp), [], 5, c);
    imdata = reshape(double(imdata), [], 1, c);
    t_svdstart=tic;
    sens = complex(zeros(x*y*z, c), zeros(x*y*z, c));
    parfor i=1:size(imdata_comp,1),
        [U,S,V]=svd(squeeze(imdata_comp(i,:,:)), 'econ');
        sens(i,:) = V(:,1).*U(1,1)./abs(U(1,1));
    end
    svdimage = sum(squeeze(imdata).*sens./abs(sens),2);
    svdimage2 = sum(squeeze(imdata_comp(:,2,:)).*sens./abs(sens),2);
    time_svd = toc(t_svdstart);
    % this is a trival but important comparative case
    name = MakeComparisonImages(reshape(svdimage2, [x,y,z]), mask, (24), 'Figures/Figure3dSVD', svdimage2)
    
    % Complex Sum
    tic;
    complexsum = sum(reshape(imdata,210,210,60,32),4);
    time_cs = toc
    name = MakeComparisonImages(complexsum, mask, (24), 'Figures/Figure3aComplexSum', svdimage)

    % Load in COMPOSER ref
    [refdata, refhdr, Bref, Rref] = loaddata_ismrmrd(strcat(base_dir, 'COMPOSERReferenceData_compare.ismrmrd'),'/images/complex/data','/images/complex/header');

    [xr,yr,zr,vr,cr]=size(refdata);
    refdata = permute(refdata,[2,1,3,4,5]); % need to remove loading permuation for correct nifti save
    % resample COMPOSER refs to imdata

    tic;
    composerinfo = niftiinfo(strcat(base_dir,'COMPOSERReferenceMagnitude.nii.gz'));
    for k=1:32,
        niftiwrite(single(real(refdata(:,:,:,k))),...
            strcat(base_dir, 'composerimages/composerreal',...
            num2str(k), '.nii'), composerinfo);
        niftiwrite(single(imag(refdata(:,:,:,k))),...
            strcat(base_dir, 'composerimages/composerimaginary',...
            num2str(k), '.nii'), composerinfo);
    end

    dir=pwd;
    cd ../Dataset1ComparativeCombos
    system('../phasecombofunctions-matlab/flirtcomposers')
    cd(dir)

    phi0resampreal = zeros([210,210,60,32]);
    phi0resampimag = zeros(size(phi0resampreal));
    for k=1:32,
        phi0resampreal(:,:,:,k) = niftiread(strcat(base_dir, 'composerimages/composer2grereal', num2str(k), '.nii.gz'));   
        phi0resampimag(:,:,:,k)  = niftiread(strcat(base_dir, 'composerimages/composer2greimaginary', num2str(k), '.nii.gz'));
    end
    phi0 = permute(complex(phi0resampreal, phi0resampimag),[2 1 3 4]);

    combdata_composer = reshape(imdata,[x,y,z,c]) .* conj(phi0) ./ abs(phi0);
    time_composer = toc
    name = MakeComparisonImages(sum(combdata_composer,4), mask, (24), 'Figures/Figure3bCOMPOSER', svdimage)

    % Virtual Reference Coil
    tic
    [combdata_vrc, refind, vc] = VRCCombination(reshape(squeeze(imdata),x,y,z,c));
    time_vrc = toc
    [name, stats] = MakeComparisonImages(sum(combdata_vrc,4), mask, (24), 'Figures/Figure3cVRC', svdimage)
   
    figmri(angle(vc(:,:,24)), [], 'gray')
    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, 'Figures/SupportingFigureVRCreferencecoil.png', '-dpng', '-r450')
    
    min_img=min(abs(reshape(squeeze(imdata),x,y,z,c)),[],4);
    h = figmri(min_img(:,:,refind(3)),[0, 3e-3],gray)
    hold on

    voxmax = sort(reshape(h.CData,[],1));
    voxceil = voxmax(end-1)
    zeroim = zeros(size(h.CData));
    zeroim(min_img(:,:,refind(3))>voxceil) = 1;
    m=figmri(zeroim,[0,3e-3])
    set(m, 'Visible', 'off')
    rbg = zeros(size(m.CData,1), size(m.CData,2), 3);
    rbg(:,:,1) = m.CData;
    [x,y] = find(min_img(:,:,refind(3))==max(max(min_img(:,:,refind(3)))))
    r = imshow(rbg)
    hold off
    set(r, 'AlphaData', zeroim)
    set(r, 'AlphaDataMapping', 'none')

    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, 'Figures/SupportingFigureVRCvoxel.png', '-dpng', '-r450')

    
    
    % BCC Combination

    % SVD to 1-channel compression for each block
    % img -> input image, dimensions: (x, y, z, chan)
    clear imdata_comp svdimage2 sens phi0 phi0resampimag phi0resampreal
    img = reshape(imdata, 210, 210, 60,32);

    N = size(img(:,:,:,1));
    block_size = [1,1,N(3)];
    rot_disp = [90,90,-90];
    tstart = cputime;
    t_espirit = tic;
    [ img_svd, Cmp_Blox ] = bcc_extractBlock( img, block_size );

    % align phase in readout

    [ img_align, Cmp_Align ] = bcc_alignRO( img, Cmp_Blox, block_size );
    % imagesc3d2(angle(img_align), N/2, 12, rot_disp, [-pi,pi])

    % align phase in phase encode

    [ img_align2, Cmp_Align2  ] = bcc_alignPE( img, Cmp_Align, block_size );
    % imagesc3d2(angle(img_align2), N/2, 22, rot_disp, [-pi,pi])

    % align phase in partition encode

    [ img_align3, Cmp_Align3 ] = bcc_alignPAR( img, Cmp_Align2, block_size );
    % imagesc3d2(angle(img_align3), N/2, 32, rot_disp, [-pi,pi])

    % ESPIRiT requires BART -> install version 0.2.06 and add to path

    cd BCC_ESPIRiT_Coil_Combine_Toolbox/bart-0.2.06/
    bart_path = [pwd];
%     system('make')
    cd ../..

    setenv('TOOLBOX_PATH', bart_path)
    addpath(strcat(getenv('TOOLBOX_PATH'), '/matlab'));
    setenv('PATH', strcat(getenv('TOOLBOX_PATH'), ':', getenv('PATH')));
    setenv('LD_LIBRARY_PATH', '');


    % ESPIRiT with virtual body coil as phase reference

    num_acs = 16;       % size of calibration k-space fopr ESPIRiT
    c = 0.2             % threshold for sensitivity mask    


    % tic
        % concatenate virtual body coil as channel 1 to serve as phase
        % reference, but weight its magnitude by 1e-6 to preserve parallel imaging
        writecfl('img', single( cat(4, 1e-6 * img_align3, img) ))              

        system('fft 7 img kspace')

        system(['ecalib -c ', num2str(c), ' -r ', num2str(num_acs), ' kspace calib'])

        system('slice 4 0 calib sens')
    % toc


    % clean up space:
    system('rm calib.hdr')
    system('rm calib.cfl')

    system('rm kspace.hdr')
    system('rm kspace.cfl')

    system('rm img.hdr')
    system('rm img.cfl')

    %
    sens_bcc = single(readcfl('sens'));

    % grab only the head array sensitivities
    sens_bcc = sens_bcc(:,:,:,2:end);      


    % coil combine SENSE R=1 fashion:
    img_espirit_bcc = sum(img .* conj(sens_bcc)./abs(sens_bcc), 4);% ./ (eps + sum(abs(sens_bcc).^2, 4));
    time_espirit = toc(t_espirit);
    imagesc3d2(angle(img_espirit_bcc), N/2, 42, rot_disp, [-pi,pi], [], 'Coil combined volume')
    %
    img_espirit_bcc(isnan(img_espirit_bcc))=0;
    name = MakeComparisonImages(img_espirit_bcc, mask, (24), 'Figures/Figure3eBCC', svdimage)

    
    % save timing to file
    if timing_mode==0
        break
    end
    
    if exist('timing.mat','file')
        load('timing.mat')
        if ~isfield(timecombo,'svd')
            timecombo.cs = [];
            timecombo.svd = [];
            timecombo.composer = [];
            timecombo.vrc = [];
            timecombo.espirit = [];
        end
        timecombo.cs = [timecombo.cs, time_cs];
        timecombo.svd = [timecombo.svd, time_svd];
        timecombo.composer = [timecombo.composer, time_composer];
        timecombo.vrc = [timecombo.vrc, time_vrc];
        timecombo.espirit = [timecombo.espirit, time_espirit];
        save('timing.mat','timecombo')
    else
        timecombo.cs = []
        timecombo.svd = [];
        timecombo.composer = [];
        timecombo.vrc = [];
        timecombo.espirit = [];
        timecombo.cs = [timecombo.cs, time_cs];
        timecombo.svd = [timecombo.svd, time_svd];
        timecombo.composer = [timecombo.composer, time_composer];
        timecombo.vrc = [timecombo.vrc, time_vrc];
        timecombo.espirit = [timecombo.espirit, time_espirit];
        save('timing.mat','timecombo')
    end
    close all
    clear 
end