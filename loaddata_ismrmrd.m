function [merged_data, hdr, Bdata, R] = loadData_ismrmrd(filelist,data_path,hdr_path, max_vol, mb, line, num2read)
    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.

    w = warning ('on','all');
    for f=1:size(filelist,1)
        strcat("loading file ", filelist(f,:))
        hdr=h5read(char(filelist(f,:)), hdr_path);

        %need to calculate data size
        slices = unique(hdr.position','rows');
        volumes = unique(hdr.acquisition_time_stamp);
        x = hdr.matrix_size(1,1);
        y = hdr.matrix_size(2,1);
        c = hdr.channels(1);
        % volumes and slices are kept together we can seperately get both nums
        % and then confirm the numbers make sense
        z = size(slices,1);
        v = size(volumes,1);
        
        try
            assert(z*v==size(hdr.position,2), "Cannot determine slices and volumes from data")
            if nargin > 3
                volumes = volumes(1:max_vol);
                v = size(volumes,1);
            end
            no_vol = false;
        catch ME
            if size(slices,1)==1
                error("no slice information given, cannot load data")
            elseif size(volumes)==1
                warning("no volume information provided, assuming volume from index")
                v = size(hdr.position,2)/z;
                if nargin > 3 && v > max_vol
                    v = max_vol;
                end
                no_vol = true;
            elseif nargin > 4
                if size(volumes,1)*mb == size(hdr.position,2)
                    % slice ordering in volumes but this data is multi-band and so timing dupli, loading proceeds normally
                    v = size(hdr.position,2)/z;
                    if nargin > 3 && v > max_vol
                        v = max_vol;
                    end
                    no_vol=false;
                end
            else
                warning("volume information incomplete, this could be due to averaging. assuming volume from index")
                v = size(hdr.position,2)/z;
                if nargin > 3 && v > max_vol
                    v = max_vol;
                end
                no_vol = true;
            end
        end
        if nargin < 4
           max_vol=inf; 
        end
        if nargin == 6
            x=1;
        elseif nargin==7
            x=num2read;
        end
        data = complex(zeros([x,y,z,v,c]),zeros([x,y,z,v,c])); %x,y,z,v,c
        images_in=0;
        for s=1:size(hdr.channels,1)
            slice = find(ismember(slices, hdr.position(:,s)', 'rows')==1);
            if no_vol
                vol = find(find(ismember(hdr.position', hdr.position(:,s)', 'rows')==1)==s);
            else
                vol = find(sort(hdr.acquisition_time_stamp(ismember(hdr.position', hdr.position(:,s)', 'rows')))==hdr.acquisition_time_stamp(s));
            end
            if length(vol)==0 || vol>max_vol
                continue
            end
            if nargin > 5
                raw_data = h5read(char(filelist(f,:)),data_path, [line 1 1 1 s], double([x,y,1,c,1]));
            else
                raw_data = h5read(char(filelist(f,:)),data_path, [1 1 1 1 s], double([x,y,1,c,1]));
            end
            raw_data = complex(raw_data.real, raw_data.imag);
            data(:,:,slice,vol,:)=permute(raw_data,[1,2,3,5,4]);
            images_in=images_in+1;
            if images_in==(z*v)
                break
            end
        end
        
        if f>1
            [~,Rnew]=calcpositions_ismrmrd(hdr);
            assert(all(all(R==Rnew)), "data files contain different image sets")
        end
        [Bdata,R] = calcpositions_ismrmrd(hdr);
        if nargin > 5
            Bdata = reshape(Bdata, [], y, z, 3);
            Bdata = reshape(Bdata(:,line:line+num2read-1,:,:), [],3);
        end
        
        if f==1
            merged_data=data;
            clear data 
        else
            assert(all(size(merged_data(:,:,:,1,:))==size(data(:,:,:,1,:))), "data files contain different image sets");
            merged_data = cat(4, merged_data, data);
        end
    end
    % The dicom style data is flipped in y and z relative to the basis. I
    % choose to fix it here but it could also be fixed in the coordinate
    % generation.
    merged_data = permute(merged_data,[2,1,3,4,5]);
end

