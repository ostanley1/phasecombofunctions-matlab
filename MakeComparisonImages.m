function [name, stats]= MakeComparisonImages(image, mask, slices, name, svdimage)
    % Assess quality ratio for each fit in files over a mask
    % image - combined data [x,y,z]
    % mask - mask to assess quality over [x,y,z]
    % slices - array of slices to plot
    % name - name of output figure
    % svdimage - svd combined image [x*y*z]
    
    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.
    

    quality = abs(image)./abs(reshape(svdimage, size(image)));
    magnitude = abs(svdimage);
    cleaned_mask = (mask(:)>0).*(magnitude(:)>(0.03*median(magnitude(:))));
    stats = [mean(quality(cleaned_mask(:)>0)) std(quality(cleaned_mask(:)>0))];
    stats = [stats stats(2)/stats(1)*100]

    figure('Position', [10 10 900 600])
    figmri(image(:,:,slices).*mask(:,:,slices), [], 'gray')
    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, strcat(name, 'mag.png'), '-dpng', '-r450')

    figure('Position', [10 10 900 600])
    figmri(angle(image(:,:,slices)), [], 'gray')
    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, strcat(name, 'phase.png'), '-dpng', '-r450')

    phaseuw = unwrap3dA(single(angle(image)), mask>0);
    figure('Position', [10 10 900 600])
    figmri(phaseuw(:,:,slices).*mask(:,:,slices), [-10, 10], 'gray')
    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, strcat(name, 'phaseuw.png'), '-dpng', '-r450')

    figure('Position', [10 10 900 600])
    figmri(quality(:,:,slices).*mask(:,:,slices), [0 1])
    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, strcat(name, 'quality.png'), '-dpng', '-r450')
end

