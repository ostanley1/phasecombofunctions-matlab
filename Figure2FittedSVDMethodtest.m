% Generates all figures for Figure 2 and all Fitted SVD method figures for
% Figure 3. 
% 
% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.


%% Load in Human B1+ Data
base_dir = '../Dataset1ComparativeCombos/'

filelist = {strcat(base_dir,'RelB1Data_compare.ismrmrd');...
    strcat(base_dir,'AbsB1Data_compare.ismrmrd')}

[data, hdr, Bshim, Rshim] = loaddata_ismrmrd(filelist,'/images/complex/data','/images/complex/header');

%% Call FittedSVDSensitivities systematically

o = 1:10;
fit_mask = 5:5:45;
minimax_mask = 5:5:45;
[in_o, in_fit, in_minimax] = ndgrid(o, fit_mask, minimax_mask);
input_params = [in_o(:), in_fit(:), in_minimax(:)];

fitresults = []*size(input_params,1);
minimax_values = zeros(size(minimax_mask,2),2);
minimax_values(:,1) = minimax_mask;
for n=1:size(input_params,1)
    idx = minimax_mask==input_params(n,3);                                                                                                                                                                       
    if minimax_values(idx,2)==0
        fitresults = [fitresults, FittedSVDSensitivities(data, Bshim, input_params(n,:), 'FittedSVDprescan')]
        minimax_values(idx,2)=n
    else
        fitresults = [fitresults, FittedSVDSensitivities(data, Bshim, input_params(n,:), 'FittedSVDprescan',...
                                                         fitresults(minimax_values(idx,2)).minimax_weights)]
    end
end
save('Figure2Fits.mat', 'fitresults')
%% Load in test data
test_string = strcat(base_dir, 'TargetGREData%i_compare.ismrmrd');
for i=1:5
    test{i,1}=sprintf(test_string, i);
end
[imdata, imhdr, Bim, Rim] = loaddata_ismrmrd(test,'/images/complex/data','/images/complex/header');
[x,y,z,v,c]=size(imdata);

%% Load in mask and confirm orientation
sos = sqrt(sum(real(imdata).^2 + imag(imdata).^2,5));
mask = rot90(niftiread(strcat(base_dir, 'ComparativeComboBrainMask.nii.gz')),-1);
figure
figmri((mask*5+1).*sos(:,:,:,1))

%% Run SVD for quality ratio
% create target set from svd
imdata = reshape(double(imdata), [], v, c);
sens = complex(zeros(x*y*z,c),zeros(x*y*z,c));
parfor i=1:size(imdata,1)
    [U,~,V]=svd(squeeze(imdata(i,:,:)));
    sens(i,:) = V(:,1).*U(1,1)./abs(U(1,1));
end
svdimage = sum(squeeze(imdata(:,1,:)).*sens./abs(sens),2);
imdata = reshape(imdata(:,1,:), x,y,z,1, c);

%% Call AssessfSVD for array of fit quality
load('Figure2Fits.mat')

[quality_array] = AssessfSVD(fitresults, imdata, mask, Bim, svdimage);
save('Figure2Results.mat', 'quality_array')

%% present fit quality results
load('Figure2Results.mat')

quality_array(:,6) = 100*quality_array(:,5)./quality_array(:,4);

quality_mean_im = reshape(quality_array(:,4),10,9,9);
quality_cov_im = reshape(quality_array(:,6),10,9,9);

o=1:10;
o_labels = mat2cell(o, 1, length(o));
fit_mask=5:5:45;
fit_mask_labels = mat2cell(fit_mask, 1, length(fit_mask));

name = MakeSummaryStatFigure(quality_mean_im(:,:,4), [], fit_mask_labels, o_labels, [0.5, 0.6, 0.7, 0.8, 0.9, 1],...
                             'a)', 'Fit Mask', 'Order', 'Quality Ratio', 'Figures/Figure2aQualityRationMean.png')
name = MakeSummaryStatFigure(quality_cov_im(:,:,4), [], fit_mask_labels, o_labels, [0, 5, 10, 15, 20, 25],...
                             'b)', 'Fit Mask', 'Order', '%', 'Figures/Figure2bQualityRationCoV.png') 
                         
                         
%% Make combination images
load('Figure2Fits.mat')
ind = sub2ind([10,9,9], 6, 4, 4);
fitresults(ind)

[names, stats] = MakefSVDImages(fitresults(ind), imdata, mask,(24), Bim, 'Figures/Figure2FittedSVDprescan', svdimage)

% supporting information full slice save
[names, stats] = MakefSVDImages(fitresults(ind), imdata, ones(size(mask)),(1:z), Bim, 'Figures/Figure2FittedSVDprescanfull', svdimage)
%% Show SNR based masking
sdiagratio = reshape(fitresults(1).mask,32,32,32);
sosshim = sqrt(sum(real(data).^2 + imag(data).^2,5));
for m=10:10:40
    figure
    forig=figmri(10*sosshim(:,:,6:4:end-5,end-1)+((sdiagratio(:,:,6:4:end-5)>m)), [], 'gray')
    hold on
    shimhull = convhulln(double(fitresults(1).coords(sdiagratio(:)>m,:)));
    imhull = reshape(inhull(fitresults(1).coords, fitresults(1).coords(sdiagratio(:)>m,:), shimhull),32,32,32);
    siz = size(imhull(:,:,6:4:end-5));
    n=2;
    imhull = reshape(imhull(:,:,6:4:end-5), [siz(1:2) n siz(3)/n]);
    imhull = permute(imhull, [1 3 2 4]);
    siz = size(imhull);
    siz(end+1:4) = 1;
    imhull = double(reshape(imhull, [prod(siz(1:2)) prod(siz(3:4))]));
    imhull(imhull(:)==1)=0.5;
    imhull(imhull(:)==0)=1;
    set(forig, 'AlphaData', imhull)
    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, strcat('Figures/Figure2B1shimmask',num2str(m), '.png'), '-dpng', '-r450')
end


%% Get timing of the fsvd method - prescan and image
timing_mode = 0 %change if you wish to time all combos
for count=1:5
    count

    fsvdprescan_start = tic;
    order = 6;
    fit_mask = 20;
    minimax_mask = 20;
    fitresults_shim = FittedSVDSensitivities(data, Bshim, [order, fit_mask, minimax_mask], 'Figure2timed')

    basisfit = hhbasis(fitresults_shim.order, Bim(:,1), Bim(:,2), Bim(:,3), 1, 'solid');

    shimhull = convhulln(double(fitresults_shim.coords(fitresults_shim.mask>fitresults_shim.fit_mask,:)));
    imhull = inhull(Bim, fitresults_shim.coords(fitresults_shim.mask>fitresults_shim.fit_mask,:), shimhull);
    [~, idx] = bwdist(reshape(imhull,size(mask,1), size(mask,2), size(mask,3)));

    Mcalc=basisfit*squeeze(fitresults_shim.coil_weights);
    Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
    image = sum(reshape(imdata(:,:,:,1,:),[],32).*Mcalc./abs(Mcalc),2);
    time_fSVDprescan = toc(fsvdprescan_start);
    
    [names, stats] = MakefSVDImages(fitresults_shim, imdata, mask,(24), Bim, 'Figures/Figure2PrescanFit', svdimage)

    % timing of the fsvd method - images
    compdata_string = strcat(base_dir, 'ComparisonGREData%i_compare.ismrmrd');
    for i=1:5
        compdata{i,1}=sprintf(compdata_string, i);
    end

    [imdata_comp, ~, Bim_comp, ~] = loaddata_ismrmrd(compdata,'/images/complex/data','/images/complex/header');
    
    assert(size(imdata_comp,4)==5)
    fsvdimage_start=tic;
    order = 6;
    fit_mask = 20;
    minimax_mask = 20;
    fitresults_image = FittedSVDSensitivities(imdata_comp, Bim_comp, [order, fit_mask, minimax_mask], 'Figure2Imagetimed')

    basisfit = hhbasis(fitresults_image.order, Bim_comp(:,1), Bim_comp(:,2), Bim_comp(:,3), 1, 'solid');

    shimhull = convhulln(double(fitresults_image.coords(fitresults_image.mask>fitresults_image.fit_mask,:)));
    imhull = inhull(Bim, fitresults_image.coords(fitresults_image.mask>fitresults_image.fit_mask,:), shimhull);
    [~, idx] = bwdist(reshape(imhull,size(mask,1), size(mask,2), size(mask,3)));

    Mcalc=basisfit*squeeze(fitresults_image.coil_weights);
    Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
    image = sum(reshape(imdata_comp(:,:,:,1,:),[],32).*Mcalc./abs(Mcalc),2);
    time_fSVDimage = toc(fsvdimage_start);
    
    [names, stats] = MakefSVDImages(fitresults_image, imdata, mask,(24), Bim, 'Figures/Figure2ImageFit', svdimage)

     % save timing to file
    if timing_mode==0
        break
    end   
    
    % save timing to file

    if exist('timing.mat','file')
        load('timing.mat')
        if ~isfield(timecombo,'fSVDprescan')
            timecombo.fSVDprescan = [];
            timecombo.fSVDimage = [];
        end
        timecombo.fSVDprescan = [timecombo.fSVDprescan, time_fSVDprescan];
        timecombo.fSVDimage = [timecombo.fSVDimage, time_fSVDimage];
        save('timing.mat','timecombo')
    else
        timecombo.fSVDprescan = [time_fSVDprescan];
        timecombo.fSVDimage = [time_fSVDimage];
        save('timing.mat','timecombo')
    end

end

%% Calculate time cost of applying the fit to the data
time_apply = [];
order = 6;
fit_mask = 20;
minimax_mask = 20;
fitresults = FittedSVDSensitivities(data, Bshim, [order, fit_mask, minimax_mask], 'FitApplicationtimed')

for i=1:5
    t=tic;
    basisfit = hhbasis(fitresults.order, Bim(:,1), Bim(:,2), Bim(:,3), 1, 'solid');

    shimhull = convhulln(double(fitresults.coords(fitresults.mask>fitresults.fit_mask,:)));
    imhull = inhull(Bim, fitresults.coords(fitresults.mask>fitresults.fit_mask,:), shimhull);
    [~, idx] = bwdist(reshape(imhull,size(mask,1), size(mask,2), size(mask,3)));

    Mcalc=basisfit*squeeze(fitresults.coil_weights);
    Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
    image = sum(reshape(imdata(:,:,:,1,:),[],32).*Mcalc./abs(Mcalc),2);
    time_apply = [time_apply toc(t)];
end