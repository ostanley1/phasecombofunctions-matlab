function [X, B, basisout, reslist, d, time ] = runiterativefit( order, soda, svdsens, weight, coords)
    %Runs iterative fit of svd results at a specified order
    %order = solid harmonic order
    %soda = image soda 
    %svdsens = input snesitivities to fit (masked already)
    %weight = weights for sensitivities (zerod)
    %coords = if no soda can supply coords [x*y*z,3]

    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.
    
    if nargin>4,
        xl = coords(:,1);
        yl = coords(:,2);
        zl = coords(:,3);
    else
        pos=patientframe(soda,[]);

        xl = pos(1,:)';
        yl = pos(2,:)';
        zl = pos(3,:)';
    end
        
    basisout = hhbasis(order, reshape(xl,1,[])', reshape(yl,1,[])', reshape(zl,1,[])', 1, 'solid');
    basisw = bsxfun(@times, basisout(weight~=0,:), weight(weight~=0));
    
    maxiter=10000;
    b = svdsens; % %stays constant no scaling applied ;
    B = b; %will be solution after scaling applied
    reslist = zeros(1,maxiter);
    reslist(1) = realmax;
    d = complex(zeros([1,size(b,1)]), zeros([1,size(b,1)]));
    fit_time = tic;
    for j=1:maxiter
         
         X = basisw\B;
         M = basisw*X;

         [w, id] = lastwarn;
         if contains(id, 'rank') || contains(id, 'Rank')
             X = zeros(size(X));
             warning('')
             break
         end
         
         
         res = B - M;
         rss = sum(abs(res(:)).^2);
         if rss > 0.9999*reslist(j)
             reslist(j+1)=rss;
             reslist = reslist(1:j+1);
             j
             order
             break
         end
         reslist(j+1)=rss;

         %%Calculate scaling factor
         d=(sum(M.*conj(b),2)./sum(b.*conj(b),2)).';
         
         % need mean of d to be unity 
         d = d*sqrt(size(M,1))./sqrt(d*d'); %d*d' = voxels
         B = bsxfun(@times, b, d.');
         
         if mod(j,100)==1
             j
         end
    end
    time=toc(fit_time);
end

