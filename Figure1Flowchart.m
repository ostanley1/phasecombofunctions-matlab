% Generate coil sensitivities to accompany Fig1 Flowchart.
% 
% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% load shim data
base_dir = '../Dataset1ComparativeCombos/'

filelist = [strcat(base_dir,'RelB1Data_compare.ismrmrd');...
    strcat(base_dir,'AbsB1Data_compare.ismrmrd')]

[data, hdr, Bshim, Rshim] = loaddata_ismrmrd(filelist,'/images/complex/data','/images/complex/header');

%% load test data
test_string = strcat(base_dir, 'TargetGREData%i_compare.ismrmrd');
for i=1
    test{i,1}=sprintf(test_string, i);
end
[imdata, imhdr, Bim, Rim] = loaddata_ismrmrd(test,'/images/complex/data','/images/complex/header');
[x,y,z,v,c]=size(imdata);

%% Load in mask and confirm orientation
sos = sqrt(sum(real(imdata).^2 + imag(imdata).^2,5));
mask = rot90(niftiread(strcat(base_dir, 'ComparativeComboBrainMask.nii.gz')),-1);
figure
figmri((mask*5+1).*sos(:,:,:,1))

%% fit and svd

order = 6;
fit_mask = 20;
minimax_mask = 20;
fitresults_shim = FittedSVDSensitivities(data, Bshim, [order, fit_mask, minimax_mask], 'Figure1flowchart')

data = reshape(double(data), [], 10, c);
sens = complex(zeros(32^3, c), zeros(32^3, c));
sdiag = zeros(32^3,min(c,10));
for i=1:size(data,1),
    [U,S,V]=svd(squeeze(data(i,:,:)),'econ');
    sens(i,:) = V(:,1).*U(1,1)./abs(U(1,1));
    sdiag(i,:)=diag(S);
end

%% make figures
sens = reshape(sens, 32, 32, 32, 32); 
coils = [1, 2, 13, 20];

figure
figmri(sens(:,:,16,coils), [], gray)
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1SVDsensmag.png', '-dpng', '-r450')

figure
figmri(angle(sens(:,:,16,coils)), [], gray)
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1SVDsensphase.png', '-dpng', '-r450')

sens = reshape(sens, 32^3, 32);
gs = sum(bsxfun(@times, sens, fitresults_shim.minimax_weights.'), 2);
wsens = reshape(bsxfun(@times, sens, conj(gs)), 32,32,32,32);

figure
figmri(wsens(:,:,16,coils), [], 'gray')
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1minimaxsensmag.png', '-dpng', '-r450')

figure
figmri(angle(wsens(:,:,16,coils)), [], 'gray')
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1minimaxsensphase.png', '-dpng', '-r450')

basisfit = hhbasis(fitresults_shim.order, Bim(:,1), Bim(:,2), Bim(:,3), 1, 'solid');

shimhull = convhulln(double(fitresults_shim.coords(fitresults_shim.mask>fitresults_shim.fit_mask,:)));
imhull = inhull(Bim, fitresults_shim.coords(fitresults_shim.mask>fitresults_shim.fit_mask,:), shimhull);
[~, idx] = bwdist(reshape(imhull,size(mask,1), size(mask,2), size(mask,3)));

Mcalc=basisfit*squeeze(fitresults_shim.coil_weights);
Mcalc(imhull~=1,:)=Mcalc(idx(imhull~=1),:);
Mcalc = reshape(Mcalc, 210, 210, 60, 32);

figure
figmri(Mcalc(:,:,30,coils), [0, 0.1], 'gray')
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1imagesensmag.png', '-dpng', '-r450')

figure
figmri(angle(Mcalc(:,:,30,coils)), [], 'gray')
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1imagesensphase.png', '-dpng', '-r450')

image = squeeze(imdata).*Mcalc./abs(Mcalc);

figure
figmri(angle(image(:,:,30,coils)), [], 'gray')
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1alignedphase.png', '-dpng', '-r450')


figure
figmri(angle(sum(image(:,:,30,:), 4)), [], 'gray')
colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
print(gcf, 'Figures/Figure1combinedslice.png', '-dpng', '-r450')
