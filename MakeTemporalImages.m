function [name, stats, image]= MakeTemporalImages(image, mask, slices, name, refimage)
    % Calculate and plot temporal phase standard deviation over a mask
    % image - combined data [x,y,z,v]
    % mask - mask to assess quality over [x,y,z]
    % slices - array of slices to plot
    % name - name of output figure
    % refimage - compare to image in ratio of noise [x,y,z,v]
    
    % Copyright (c) 2021 Olivia Stanley
    % 
    % Permission is hereby granted, free of charge, to any person obtaining a copy
    % of this software and associated documentation files (the "Software"), to deal
    % in the Software without restriction, including without limitation the rights
    % to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    % copies of the Software, and to permit persons to whom the Software is
    % furnished to do so, subject to the following conditions:
    % 
    % The above copyright notice and this permission notice shall be included in all
    % copies or substantial portions of the Software.
    % 
    % THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    % IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    % FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    % AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    % LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    % OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    % SOFTWARE.
    [x,y,z,v]=size(image);

    image = reshape(unwrap(angle(bsxfun(@times, image,conj(image(:,:,:,1)))), [], 4), x*y*z,v);
    image = reshape(detrend(image),x,y,z,v);
    image_std = nanstd(image, [], 4); % nan is due to BCC combo
    
    if nargin > 4
        refimage = reshape(unwrap(angle(bsxfun(@times, refimage,conj(refimage(:,:,:,1)))), [], 4), x*y*z,v);
        refimage = reshape(detrend(refimage),x,y,z,v);
        refimage_std = nanstd(refimage, [], 4); % nan is due to BCC combo
        
        image_std = image_std./refimage_std;
        disp('Using ratio to reference image')
    end

    magnitude = mean(abs(image), 4);
    cleaned_mask = (mask(:)>0).*(magnitude(:)>(0.03*median(magnitude(:))));

    stats = [mean(image_std(cleaned_mask(:)>0)), std(image_std(cleaned_mask(:)>0))];

    figure('Position', [10 10 900 600])
    if nargin >4
        clim=[0,4]
    else
        clim=[0,0.3]
    end
    figmri(image_std(:,:,slices).*mask(:,:,slices),clim,'parula')
    colorbar('off')
    set(gca, 'XColor', 'none', 'YColor', 'none')
    print(gcf, name, '-dpng', '-r450')
end

