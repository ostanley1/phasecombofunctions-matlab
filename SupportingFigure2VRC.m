% Generate image of VRC reference voxel to demonstrate how it may need adjustment at high field

% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.


close all

base_dir = '../Dataset1ComparativeCombos/'

[imdata, imhdr, Bim, Rim] = loaddata_ismrmrd(strcat(base_dir, 'TargetGREData1_compare.ismrmrd'),'/images/complex/data','/images/complex/header');
[x,y,z,v,c]=size(imdata);

mask = rot90(niftiread(strcat(base_dir, 'ComparativeComboBrainMask.nii.gz')),-1);

[combdata_vrc, refind] = VRCCombination(reshape(squeeze(imdata),x,y,z,c));

min_img=min(abs(reshape(squeeze(imdata),x,y,z,c)),[],4);
h = figmri(min_img(:,:,refind(3)),[0, 3e-3],gray)
hold on

voxmax = sort(reshape(h.CData,[],1));
voxceil = voxmax(end-1)
zeroim = zeros(size(h.CData));
zeroim(min_img(:,:,refind(3))>voxceil) = 1;
m=figmri(zeroim,[0,3e-3])
set(m, 'Visible', 'off')
rbg = zeros(size(m.CData,1), size(m.CData,2), 3);
rbg(:,:,1) = m.CData;
[x,y] = find(min_img(:,:,refind(3))==max(max(min_img(:,:,refind(3)))))
r = imshow(rbg)
hold off
set(r, 'AlphaData', zeroim)
set(r, 'AlphaDataMapping', 'none')

colorbar('off')
set(gca, 'XColor', 'none', 'YColor', 'none')
saveas(gcf, 'Figures/SupportingFigureVRCvoxel.png')
