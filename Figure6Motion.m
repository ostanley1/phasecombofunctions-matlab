% Generates all figures for Figure 5. 
% 
% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

base_dir = '../Dataset3Motion/'

%% Load in Prescan Dataset after motion occured
shimfiles = {strcat(base_dir, 'RelB1Data_postmotion.ismrmrd');...
                      strcat(base_dir, 'AbsB1Data_postmotion.ismrmrd')};

[shimdata, shimhdr, Bshim, Rshim] = loaddata_ismrmrd(shimfiles,'/data/images/data','/data/images/header');

%% Call FittedSVDSensitivities
o = 6
fit_mask = 20 
minimax_mask = 20

fitresults=FittedSVDSensitivities(shimdata, Bshim, [o,fit_mask,minimax_mask], 'Figure5Motion')

%% Load in Prescan Dataset prior to motion occuring
shimfiles_nomo = {strcat(base_dir, 'RelB1Data_premotion.ismrmrd');...
                      strcat(base_dir, 'AbsB1Data_premotion.ismrmrd')};

[shimdata_nomo, shimhdr, Bshim_nomo, Rshim] = loaddata_ismrmrd(shimfiles_nomo,'/data/images/data','/data/images/header');

%% Call FittedSVDSensitivities

fitresults_nomo = FittedSVDSensitivities(shimdata_nomo, Bshim_nomo, [o,fit_mask,minimax_mask], 'Figure5NoMotion')

%% Load in target set collected before motion
test_string = strcat(base_dir, 'TargetGREData%i_premotion.ismrmrd');
for i=1:5
    test{i,1}=sprintf(test_string, i);
end

[imdata, imhdr, Bim, Rim] = loaddata_ismrmrd(test,'/data/images/data','/data/images/header');
[x,y,z,v,c]=size(imdata);
%% Load in and confirm mask orientation
sos = sqrt(sum(real(imdata).^2+imag(imdata).^2,5));
mask = rot90(niftiread(strcat(base_dir, 'MotionDataBrainMask')),-1);

figmri((mask*10+1).*sos(:,:,:,1))

%%
% create target set from svd
imdata = reshape(double(imdata), [], v, c);
sens = complex(zeros(x*y*z,c), zeros(x*y*z,c));
parfor i=1:size(imdata,1)
    [U,~,V]=svd(squeeze(imdata(i,:,:)));
    sens(i,:) = V(:,1).*U(1,1)./abs(U(1,1));
end
svdimage = sum(squeeze(imdata(:,1,:)).*sens./abs(sens),2);
imdata = reshape(imdata, x,y,z,v, c);

%% Make output images

[names, stats] = MakefSVDImages(fitresults, imdata(:,:,:,1,:), mask,[24], Bim, 'Figures/Figure5MotionTestPostMotion', svdimage);
[names, stats_nomo] = MakefSVDImages(fitresults_nomo, imdata(:,:,:,1,:), mask,[24], Bim, 'Figures/Figure5MotionTestPreMotion', svdimage);
