% Generates all figures for Figure 4. 
% 
% Copyright (c) 2021 Olivia Stanley
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%% load in shim data
base_dir = '../Dataset2OccipitalCoil/'

filelist = {strcat(base_dir,'RelB1Data_opcoil.ismrmrd');...
    strcat(base_dir,'AbsB1Data_opcoil.ismrmrd')}

[shimdata, shimhdr, Bshim, Rshim] = loaddata_ismrmrd(filelist,'/data/images/data','/data/images/header');

%% Call FittedSVDSensitivities 
o = 6
fit_mask = 20 
minimax_mask = 20 

fitresults = FittedSVDSensitivities(shimdata, Bshim, [o, fit_mask, minimax_mask], 'Figure4OPCoil')
        
%% Load in test data
filelist = strcat(base_dir, 'TargetEPIData.ismrmrd');

[imdata, imhdr, Bim, Rim] = loaddata_ismrmrd(filelist,'/images/complex/data','/images/complex/header', 10,3);
[x,y,z,v,c]=size(imdata)
%% Load in and confirm mask orientation
sos = sqrt(sum(real(imdata).^2+imag(imdata).^2,5));
mask= niftiread(strcat(base_dir, 'OPCoilDataBrainMask.nii.gz'));
mask(:,:,1:10)=0;
mask(:,:,51:end)=0;
mask=double(rot90(mask,-1));
figmri((mask*10+1).*sos(:,:,:,1))

%% Run SVD
imdata = reshape(double(imdata), [], v, c);
sens = complex(zeros(x*y*z,c), zeros(x*y*z, c));
parfor i=1:size(imdata,1),
    [U,~,V]=svd(squeeze(imdata(i,:,:)));
    sens(i,:) = V(:,1).*U(1,1)./abs(U(1,1));
end
svdimage = sum(squeeze(imdata(:,1,:)).*sens./abs(sens),2);
imdata = reshape(imdata,104,104,54,[],32);

%% Make pictures

names = MakefSVDImages(fitresults, imdata, mask,[35:3:45], Bim, 'Figures/Figure4OPCoil', svdimage)
